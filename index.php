<?php

    require_once 'User.php';

    $user = new User;
    

    $users = $user->all();

    if (isset($_POST['delete'])) {
        $id = $_POST['delete'];
        
        $user->delete($id);
        header("location:index.php");
    }

    if (isset($_GET['edit'])) {
        $id = $_GET['edit'];

        // echo $id;
        $olduser =  $user->one($id);
        // header("location:index.php");
    }

    if (isset($_POST['update'])) {
        $id = $_POST['id'];

        $user->update($id,$_POST);

        header("location:index.php");

    }
    
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <title>Users</title>
</head>

<body>
    <div class="container mx-auto mt-8">
        <!-- component -->
        <div class="text-gray-900 bg-gray-200">
            <div class="p-4 flex">
                <h1 class="text-3xl">
                    Users
                </h1>
            </div>
            <div class="px-3 py-4 flex justify-center">
                <table class="w-full text-md bg-white shadow-md rounded mb-4">
                    <tbody>
                        <tr class="border-b">
                            <th>avatar</th>
                            <th class="text-left p-3 px-5">Name</th>
                            <th class="text-left p-3 px-5">Email</th>
                            <th class="text-left p-3 px-5">Role</th>
                            <th></th>
                        </tr>

                        <?php foreach($users as $u) : ?>

                        <tr class="border-b hover:bg-orange-100 bg-gray-100">
                            <th class="p-3 rounded"><img src="https://i.pravatar.cc/70/?img=<?php echo $u['id']; ?>"
                                    alt=""></th>
                            <td class="p-3 px-5"><?php echo $u['name']; ?></td>
                            <td class="p-3 px-5"><?php echo $u['email']; ?></td>
                            <td class="p-3 px-5">
                                <?php echo $u['role']; ?>
                            </td>
                            <td class="p-3 px-5 mt-5 flex justify-end ">
                                <a id="edit" href="?edit=<?php echo $u['id'] ?>"
                                    class="text bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline mr-3">Edit</a>

                                <form action="" method="POST">
                                    <input type="hidden" name="delete" value="<?php echo $u['id']; ?>">
                                    <button type="submit"
                                        class="text bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline mr-3">Delete</button>

                                </form>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php if(isset($olduser)) : ?>
            <form action="" method="POST"  class="w-full max-w-lg mt-8">
                <input type="hidden" value="<?php echo $olduser['id']; ?>" name="id"> 
                <div class="flex flex-wrap -mx-3 mb-6">
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                            for="grid-first-name">
                            First Name
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                            id="grid-first-name" type="text" value="<?php echo $olduser['name']; ?>" name="name">
                    </div>
                    <div class="w-full md:w-1/2 px-3">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                            for="grid-last-name">
                            Email
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            id="grid-last-name" type="text" value="<?php echo $olduser['email']; ?>" name="email">
                    </div>
                </div>
                <select name="role" id="" class="my-5">
                    <option value="user">user</option>
                    <option value="admin">admin</option>
                </select>
                <div class="flex flex-wrap -mx-3 mb-6">
                    <div class="w-full px-3">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                            for="grid-password">
                            Password
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            id="grid-password" type="password" value="<?php echo $olduser['password']; ?>" name="password">
                    </div>
                </div>
                <button
                    type="submit" name="update"
                    class="text bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline mr-3">Update</button>
                <a href="index.php"
                    class="text bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline mr-3">Cancel</a>
            </form>
        <?php endif; ?>

    </div>


</body>

</html>