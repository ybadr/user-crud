<?php

    class User 
    {   
        private $pdo;

        public function __construct()
        {
            try {
                $this->pdo = new PDO('mysql:host=localhost;dbname=test;','admin','admin123');

            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }

        public function all()
        {
            $stmt = $this->pdo->prepare('SELECT * FROM `users`');

            $stmt->execute();

            return $stmt->fetchAll();
        }

        public function one($id){
            $stmt = $this->pdo->prepare('SELECT * FROM `users` WHERE id = :id');
            $stmt->bindValue('id',$id);

            $stmt->execute();

            return $stmt->fetch();
        }
         
        public function delete($id)
        {
            $stmt = $this->pdo->prepare('DELETE FROM `users` WHERE id = :id');
            $stmt->bindValue('id',$id);
            $stmt->execute();

        }

        public function update($id, $data)
        {
            $stmt = $this->pdo->prepare("UPDATE `users` SET `name`=:name,`email`=:email,`role`=:role ,`password`=:password WHERE id = :id");
            $stmt->bindValue('id',$id);
            $stmt->bindValue('name',$data['name']);
            $stmt->bindValue('email',$data['email']);
            $stmt->bindValue('role',$data['role']);
            $stmt->bindValue('password',$data['password']);
            $stmt->execute();

        }
    }